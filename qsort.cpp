#include <iostream>
#include <cstdlib>//for "exit()" on some systems
#include <vector>

using namespace std;

int count = 0;



int partition(auto& A, int l, int r)
{	
	int pivot, i, j;
    pivot = A[l + (r - l) / 2];
    i = l - 1;
    j = r + 1;
    while (true)
    {
 
        while (true)
        {
			count++;
			if (A[i] < pivot)
				i++;
			else
				break;
		}
       
        while (true)
        {
			count++;
			if (A[j] > pivot)
				j--;
			else
				break;
		}	
		
	
			
		
        if (i >= j)
            return j;
        swap (A[i],A[j]);
	}
     
}

int quickSort(auto& A, int l, int r)
{
  //int count = 0;
  int s;

  
  //... 
  
  if (l < r)
	{
		
		s = partition(A, l, r);
		quickSort(A, l, s);
		quickSort(A, s + 1, r);
	}
  
  return count;
}


int main()
{
  vector<int> inputs;
  int input;

   cerr<<"Welcome to \"quickSort Analysis\". We first need some input data."<<endl;
   cerr<<"To end input type Ctrl+D (followed by Enter)"<<endl<<endl;

   
 
    while(cin>>input)//read an unknown number of inputs from keyboard
    {
       inputs.push_back(input);
    }

   cerr<<endl<<"|  Number of inputs | Number of comparisons |"<<endl;
   cerr<<"|\t"<<inputs.size();
   cout<<"| "<<quickSort(inputs, 0, inputs.size() - 1);
    
   cerr<<"\t|"<<endl<<endl<<"Program finished."<<endl<<endl;

    return 0;
}
